# Mobile Core App

> Mobile Core App is a starting project with a simple structure.

# Features
- Log in,Log out,Register & Reset Account User 
- Light & Dark Mode
- Multi Langauage (En,Kh)
- Dynamic Connection With MongoDB
- Print with Wifi/Network/Bluetooth Printer

# Lib File Structure

```
.
├── core
│   ├── models
│   │   ├── auth
│   │   │   ├── login_model.dart
│   │   │   ├── login_model.g.dart
│   │   │   ├── reset_password_model.dart
│   │   │   ├── reset_password_model.g.dart
│   │   │   ├── sign_up_model.dart
│   │   │   ├── sign_up_model.g.dart
│   │   │   ├── user_profile_model.dart
│   │   │   └── user_profile_model.g.dart
│   │   ├── dashboard
│   │   │   └── dashboard_model.dart
│   │   ├── select-option
│   │   │   ├── select_option_model.dart
│   │   │   └── select_option_model.g.dart
│   │   ├── setting
│   │   │   ├── setting_model.dart
│   │   │   └── setting_model.g.dart
│   │   └── user
│   │       ├── email_model.dart
│   │       ├── email_model.g.dart
│   │       ├── user_model.dart
│   │       └── user_model.g.dart
│   ├── providers
│   │   ├── auth_provider.dart
│   │   ├── connection_provider.dart
│   │   ├── dashboard_provider.dart
│   │   ├── menu_provider.dart
│   │   ├── profile_provider.dart
│   │   ├── reset_password_provider.dart
│   │   ├── setting_provider.dart
│   │   ├── sign_up_provider.dart
│   │   ├── theme_provider.dart
│   │   └── verify_account_provider.dart
│   ├── screens
│   │   ├── connecting.dart
│   │   ├── dashboard.dart
│   │   ├── drawer.dart
│   │   ├── error_404.dart
│   │   ├── home.dart
│   │   ├── login.dart
│   │   ├── page_structure.dart
│   │   ├── print_preview.dart
│   │   ├── profile.dart
│   │   ├── reset_password_by_phone.dart
│   │   ├── reset_password.dart
│   │   ├── setting.dart
│   │   ├── sign_up.dart
│   │   ├── verified_account.dart
│   │   └── verify_account.dart
│   ├── services
│   │   ├── select_options
│   │   │   └── static_options.dart
│   │   └── login_services.dart
│   ├── storages
│   │   ├── auth_storage.dart
│   │   ├── connection_storage.dart
│   │   ├── printer_paper_size_storage.dart
│   │   ├── printer_storage.dart
│   │   └── user_storage.dart
│   ├── utils
│   │   ├── alert.dart
│   │   ├── block_ui.dart
│   │   ├── constants.dart
│   │   ├── core_convert_date_time.dart
│   │   ├── core_model_converter.dart
│   │   ├── custom_form_build_style.dart
│   │   ├── custom_login_input.dart
│   │   ├── device_orientation.dart
│   │   ├── get_form_error_text.dart
│   │   ├── number_converter.dart
│   │   ├── share_feature.dart
│   │   ├── static_theme.dart
│   │   └── string_converter.dart
│   ├── widgets
│   │   ├── connection
│   │   │   └── connection_form.dart
│   │   ├── dashboard
│   │   │   ├── dashboard_grid_list.dart
│   │   │   └── dashboard_grid.dart
│   │   ├── login
│   │   │   └── login_form.dart
│   │   ├── profile
│   │   │   └── edit_profile.dart
│   │   ├── reset-new-password
│   │   │   └── reset_new_password_form.dart
│   │   ├── reset-password
│   │   │   └── reset_password_form.dart
│   │   ├── sign-up
│   │   │   └── sign_up_form.dart
│   │   ├── verify-account
│   │   │   ├── verified_account_form.dart
│   │   │   └── verify_account_form.dart
│   │   ├── app_bar_widget.dart
│   │   ├── block_ui_content.dart
│   │   ├── error_data.dart
│   │   ├── real_time_connection.dart
│   │   ├── responsive.dart
│   │   ├── share_widget.dart
│   │   ├── test_printing.dart
│   │   └── waiting_data.dart
│   └── .DS_Store
├── app.dart
├── main.dart
└── route_generator.dart
```
# Changelog

## v.1.0.0

- init release

### Bug Fixes

- none

### Breaking Change

- none

# Usage

```bash
# Install dependencies
flutter pub get

# Build, watch for changes and debug the application
flutter run
```



