import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import 'package:dart_meteor/dart_meteor.dart';
// import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:easy_localization/easy_localization.dart';
import 'core/utils/static_theme.dart';
import 'route_generator.dart';
import 'core/providers/theme_provider.dart';
import 'core/screens/home.dart';
import 'core/screens/connecting.dart';
import 'core/screens/login.dart';
import 'core/storages/connection_storage.dart';
import 'core/storages/auth_storage.dart';

MeteorClient meteor = MeteorClient.connect(url: '');

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  String? _ipAddress;

  @override
  void initState() {
    super.initState();
    checkLoginTokenExist();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    // Set dynamic ip address
    _ipAddress = await ConnectionStorage().getIpAddress();
    if (_ipAddress != null) {
      setState(() {
        meteor = MeteorClient.connect(url: 'http://$_ipAddress');
      });
    }
  }

  void checkLoginTokenExist() async {
    //isUserLoginTokenExist
    String? userLoginToken = await AuthStorage().getUserLoginToken();
    DateTime expires = await AuthStorage().getUserLoginTokenExpire();
    if (userLoginToken != null) {
      meteor
          .loginWithToken(token: userLoginToken, tokenExpires: expires)
          .then((result) {
        AuthStorage().setUserLoginToken(userLoginToken: userLoginToken);
      }).catchError((_) {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: themeProvider.themeMode,
      theme: StaticTheme.theme.lightTheme,
      darkTheme: StaticTheme.theme.darkTheme,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      localizationsDelegates: [
        EasyLocalization.of(context)!.delegate,
        FormBuilderLocalizations.delegate,
      ],
      home: StreamBuilder<DdpConnectionStatus>(
        stream: meteor.status(),
        builder: (context, snapshotStatus) {
          if (snapshotStatus.hasData && _ipAddress != null) {
            return StreamBuilder<Map<String, dynamic>?>(
              stream: meteor.user(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return const Home();
                }
                return const Login();
              },
            );
          } else {
            return const Connecting();
          }
        },
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
