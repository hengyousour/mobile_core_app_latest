import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'app.dart';
import 'core/providers/auth_provider.dart';
import 'core/providers/connection_provider.dart';
import 'core/providers/dashboard_provider.dart';
import 'core/providers/profile_provider.dart';
import 'core/providers/setting_provider.dart';
import 'core/providers/theme_provider.dart';
import 'core/providers/menu_provider.dart';
import 'core/providers/sign_up_provider.dart';
import 'core/providers/verify_account_provider.dart';
import 'core/providers/reset_password_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await initializeDateFormatting();

  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('en', 'US'),
        Locale('en', 'GB'),
      ],
      path: 'assets/translations',
      fallbackLocale: const Locale('en', 'US'),
      startLocale: const Locale('en', 'US'),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (ctx) => ThemeProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => ConnectionProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => DashboardProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => ProfileProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => SettingProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => MenuProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => AuthProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => SignUpProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => VerifyAccountProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => ResetPasswordProvider(),
          ),
        ],
        child: Phoenix(
          child: const App(),
        ),
      ),
    ),
  );
}
