import 'package:flutter/foundation.dart';

class MenuProvider with ChangeNotifier {
  int _currentPage = 0;

  int get currentPage => _currentPage;

  void updateCurrentPage(int index) {
    if (index == currentPage) return;
    _currentPage = index;
    notifyListeners();
  }

  void clearCurrentPage() {
    _currentPage = 0;
    notifyListeners();
  }
}
