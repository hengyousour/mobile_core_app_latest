import 'package:dart_meteor/dart_meteor.dart';
// import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:flutter/material.dart';
import '/core/models/auth/sign_up_model.dart';
import '/core/utils/alert.dart';
import '/core/utils/constants.dart';
import '../../app.dart';

class SignUpProvider with ChangeNotifier {
  bool _obsurceText = true;
  bool get obsurceText => _obsurceText;
  bool _loading = false;
  bool get loading => _loading;
  String? _userId = "";
  String? get userId => _userId;

  void signUp({
    required SignUpModel formDoc,
    required BuildContext context,
  }) {
    _loading = true;
    meteor.call('insertUser', args: [
      {'user': formDoc}
    ]).then((result) {
      _loading = false;
      _userId = result;
      Navigator.pushReplacementNamed(context, '/verify-account');
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.errorLight,
            context: context);
        _loading = false;
        notifyListeners();
      }
    });
    notifyListeners();
  }

  void toggleSuffixIcon() {
    _obsurceText = !_obsurceText;
    notifyListeners();
  }

  void clearState() {
    _userId = "";
    notifyListeners();
  }
}
