import 'package:flutter/material.dart';
import '/core/models/dashboard/dashboard_model.dart';

class DashboardProvider with ChangeNotifier {
  final BuildContext? context;
  DashboardProvider({this.context});

  final List<DashboardModel> _dashboardList = [
    DashboardModel(
      index: 0,
      title: "Item 1",
      onPressed: (context) {
        // Navigator.of(context).pushNamed('');
      },
      iconName: Icons.add_box_rounded,
      bgColors: [
        // CommonColors.primary.withOpacity(0.15),
        // CommonColors.primary.withOpacity(0.15)
      ],
    ),
    DashboardModel(
      index: 1,
      title: "Item 2",
      onPressed: (context) {
        // Navigator.of(context).pushNamed('');
      },
      iconName: Icons.add_box_rounded,
      bgColors: [
        // CommonColors.primary.withOpacity(0.15),
        // CommonColors.primary.withOpacity(0.15)
      ],
    )
  ];
  List<DashboardModel> get dashboardList => [..._dashboardList];
}
