import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DeviceOrientationController {
  static bool isLandscape({required BuildContext context}) =>
      MediaQuery.of(context).orientation == Orientation.landscape;
  static bool isPortrait({required BuildContext context}) =>
      MediaQuery.of(context).orientation == Orientation.portrait;

  static void setOrientationPortrait() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  static void resetOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
