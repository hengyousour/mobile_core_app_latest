import 'package:flutter/material.dart';

import '../../core/utils/alert.dart';
import '../widgets/block_ui_content.dart';

class BlockUI {
  static Future<void> show({
    required BuildContext context,
    emptyData = false,
    processingData = true,
    msg = 'Look like you haven\'t made your choice yet ...',
    msgEmptyData =
        'We\'re unable to find the data that you\'re looking for ...',
    msgProcessingData = 'Please, wait while we set thing up for you!',
  }) async {
    Alert().showAlertDialog(
        content: BlockUIContent(
          emptyData: emptyData,
          processingData: processingData,
          msg: msg,
          msgEmptyData: msgEmptyData,
          msgProcessingData: msgProcessingData,
        ),
        barrierDismissible: false,
        context: context,
        actionsEnable: false,
        buttonMode: 'no-space',
        onCancel: () {},
        onAgree: () {});
    await Future.delayed(const Duration(seconds: 1));
  }

  static hide(BuildContext context) {
    Navigator.of(context, rootNavigator: true).pop();
  }
}
