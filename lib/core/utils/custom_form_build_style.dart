import 'package:flutter/material.dart';

// InputDecoration fbTextFieldStyle({
//   @required String labelText,
//   @required ThemeData theme,
//   String errorText,
// }) {
//   return InputDecoration(
//     contentPadding: EdgeInsets.only(left: 8.0),
//     fillColor: theme.scaffoldBackgroundColor,
//     filled: true,
//     labelText: labelText,
//     labelStyle: TextStyle(fontFamily: CommonFonts.body),
//     enabledBorder: OutlineInputBorder(
//       borderSide: BorderSide(
//         color: theme.iconTheme.color,
//       ),
//     ),
//     focusedBorder: OutlineInputBorder(
//       borderSide: BorderSide(
//         color: theme.primaryColor,
//       ),
//     ),
//     focusedErrorBorder: OutlineInputBorder(
//       borderSide: BorderSide(
//         color: CommonColors.danger,
//       ),
//     ),
//     errorBorder: OutlineInputBorder(
//       borderSide: BorderSide(
//         color: CommonColors.danger,
//       ),
//     ),
//     errorText: errorText,
//     errorStyle: TextStyle(color: CommonColors.danger),
//     border: OutlineInputBorder(),
//   );
// }

InputDecoration fbTextFieldStyle({
  required String labelText,
  required ThemeData theme,
  String? errorText,
}) {
  return InputDecoration(
    isDense: true,
    // floatingLabelStyle: const TextStyle(),
    labelText: labelText,
    // labelStyle: const TextStyle(),
    errorText: errorText,
    border: const OutlineInputBorder(),
  );
}

InputDecoration fbSearchableDropDownSearchBoxStyle({
  String hintText = "Search",
  IconData suffixIcon = Icons.search,
}) {
  return InputDecoration(
    isDense: true,
    hintText: hintText,
    suffixIcon: Icon(suffixIcon),
    border: const OutlineInputBorder(),
  );
}

InputDecoration fbFilterStyle({
  String? labelText,
  required ThemeData theme,
  Color? fillColor,
}) {
  return InputDecoration(
    contentPadding: const EdgeInsets.only(left: 10, top: 10),
    fillColor: fillColor ?? theme.colorScheme.background,
    filled: true,
    labelText: labelText,
    labelStyle: theme.textTheme.bodyLarge,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide.none,
    ),
  );
}

InputDecoration fbSwitchStyle() {
  return const InputDecoration(
    contentPadding: EdgeInsets.all(0.0),
    border: OutlineInputBorder(borderSide: BorderSide.none),
  );
}

InputDecoration fbLoginTextFieldStyle({
  required String labelText,
  required IconData icon,
  required ThemeData theme,
  IconData? suffixIcon,
  bool suffixIsExist = false,
  Function? onPressedSuffixIcon,
}) {
  return InputDecoration(
    errorStyle: const TextStyle(fontSize: 0.0, height: 0.0),
    border: InputBorder.none,
    contentPadding: const EdgeInsets.all(0.0),
    prefixIcon: Icon(
      icon,
    ),
    suffixIcon: (suffixIsExist)
        ? InkWell(
            onTap: onPressedSuffixIcon as void Function()?,
            child: Icon(
              suffixIcon,
            ),
          )
        : null,
    hintText: labelText,
    hintStyle: theme.textTheme.bodyLarge!.copyWith(
      color: theme.iconTheme.color!.withOpacity(0.70),
      fontWeight: FontWeight.bold,
    ),
  );
}

InputDecoration fbPhoneFieldStyle({
  required String labelText,
  required IconData icon,
  required ThemeData theme,
  IconData? suffixIcon,
  bool suffixIsExist = false,
  Function? onPressedSuffixIcon,
}) {
  return InputDecoration(
    errorStyle: const TextStyle(fontSize: 0.0, height: 0.0),
    contentPadding: const EdgeInsets.all(0.0),
    prefixIcon: Icon(
      icon,
    ),
    suffixIcon: (suffixIsExist)
        ? InkWell(
            onTap: onPressedSuffixIcon as void Function()?,
            child: Icon(
              suffixIcon,
            ),
          )
        : null,
    hintText: labelText,
    hintStyle: theme.textTheme.bodyLarge!.copyWith(
      color: theme.iconTheme.color!.withOpacity(0.70),
      fontWeight: FontWeight.bold,
    ),
  );
}

InputDecoration fbSignUpRadioGroup({
  IconData? icon,
  required ThemeData theme,
}) {
  return InputDecoration(
    prefixIcon: Icon(
      icon,
      size: 28.0,
      color: theme.iconTheme.color,
    ),
    border: InputBorder.none,
  );
}
