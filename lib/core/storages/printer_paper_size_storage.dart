import 'package:shared_preferences/shared_preferences.dart';

class PrinterPaperSizeStorage {
  // Create SharedPreferences
  final Future<SharedPreferences> _printerPaperSizePrefs =
      SharedPreferences.getInstance();

  void setPrinterPaperSize({
    required String paperSize,
  }) async {
    SharedPreferences printerPaperSizePrefs = await _printerPaperSizePrefs;
    printerPaperSizePrefs.setString('paperSize', paperSize);
  }

  Future<String?> getPrinterPaperSize() async {
    SharedPreferences printerPaperSizePrefs = await _printerPaperSizePrefs;
    return printerPaperSizePrefs.getString('paperSize');
  }

  void clearPrinterPaperSize() async {
    SharedPreferences printerPaperSizePrefs = await _printerPaperSizePrefs;
    printerPaperSizePrefs.remove('paperSize');
  }
}
