import 'package:shared_preferences/shared_preferences.dart';

class UserStorage {
  // Create SharedPreferences
  final Future<SharedPreferences> _userPrefs = SharedPreferences.getInstance();

  void setProfile({required Map<String, dynamic> userDoc}) async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.setString('id', userDoc['_id']);
    userPrefs.setString('fullName', userDoc['fullName']);
  }

  void setUser({required Map<String, dynamic> userDoc}) async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.setString('id', userDoc['_id']);
    userPrefs.setString('username', userDoc['username']);
    userPrefs.setString('fullName', userDoc['profile']['fullName']);
  }

  Future<Map<String, dynamic>> getUser() async {
    SharedPreferences userPrefs = await _userPrefs;
    Map<String, dynamic> user = {
      'id': userPrefs.getString('id'),
      'username': userPrefs.getString('username'),
      'fullName': userPrefs.getString('fullName'),
    };

    return user;
  }

  setUserImagePath(String path) async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.setString('imagePath', path);
  }

  Future<String?> getUserImagePath() async {
    SharedPreferences userPrefs = await _userPrefs;
    return userPrefs.getString('imagePath');
  }

  setLanguage({String? lang}) async {
    SharedPreferences userPrefs = await _userPrefs;
    if (lang != null) {
      await userPrefs.setString('language', lang);
    }
  }

  Future<String?> getLanguage() async {
    SharedPreferences userPrefs = await _userPrefs;
    return userPrefs.getString('language');
  }

  void clearUser() async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.remove('_id');
    userPrefs.remove('username');
    userPrefs.remove('fullName');
    userPrefs.remove('imagePath');
    userPrefs.remove('language');
  }
}
