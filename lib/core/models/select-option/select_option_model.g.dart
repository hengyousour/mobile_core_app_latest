// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_option_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SelectOptionModel _$SelectOptionModelFromJson(Map<String, dynamic> json) =>
    SelectOptionModel(
      label: json['label'] as String?,
      value: json['value'],
    );

Map<String, dynamic> _$SelectOptionModelToJson(SelectOptionModel instance) =>
    <String, dynamic>{
      'label': instance.label,
      'value': instance.value,
    };
