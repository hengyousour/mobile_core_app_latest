import 'package:json_annotation/json_annotation.dart';
part 'reset_password_model.g.dart';

@JsonSerializable()
class ResetPasswordModel {
  @JsonKey(disallowNullValue: true)
  final String? email;
  @JsonKey(disallowNullValue: true)
  final String? telephone;
  @JsonKey(disallowNullValue: true)
  final String? password;

  const ResetPasswordModel({this.email, this.telephone, this.password});

  factory ResetPasswordModel.fromJson(Map<String, dynamic> data) =>
      _$ResetPasswordModelFromJson(data);
  Map<String, dynamic> toJson() => _$ResetPasswordModelToJson(this);
}
