// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingModel _$SettingModelFromJson(Map<String, dynamic> json) => SettingModel(
      ipAddress: json['ipAddress'] as String?,
      language: json['language'] as String?,
      printer: json['printer'] as String?,
      printerPaperSize: json['printerPaperSize'] as String?,
      printerAddress: json['printerAddress'] as String?,
      printerPort: json['printerPort'] as int,
    );

Map<String, dynamic> _$SettingModelToJson(SettingModel instance) =>
    <String, dynamic>{
      'ipAddress': instance.ipAddress,
      'language': instance.language,
      'printer': instance.printer,
      'printerPaperSize': instance.printerPaperSize,
      'printerAddress': instance.printerAddress,
      'printerPort': instance.printerPort,
    };
