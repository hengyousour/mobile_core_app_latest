import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';
import '../../providers/reset_password_provider.dart';
import '../../../core/utils/get_form_error_text.dart';
import '../../../core/models/auth/login_model.dart';
import '../../../core/providers/auth_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/utils/constants.dart';
import '../../../core/utils/custom_form_build_style.dart';
import '../../../core/utils/custom_login_input.dart';
import '../../../core/utils/string_converter.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key? key,
    required ThemeData theme,
  })  : _theme = theme,
        super(key: key);

  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  final ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: <Widget>[
            Container(
              decoration: kBoxDecorationStyle(
                color: _theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: FormBuilderTextField(
                name: 'username',
                textAlignVertical: TextAlignVertical.center,
                cursorColor: _theme.iconTheme.color,
                style: TextStyle(
                  fontFamily: CommonFonts.body,
                  color: _theme.iconTheme.color,
                ),
                decoration: fbLoginTextFieldStyle(
                    labelText: "Phone Number",
                    icon: CommonIcons.user,
                    theme: _theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
            ),
            const SizedBox(height: 10.0),
            Container(
              decoration: kBoxDecorationStyle(
                color: _theme.primaryColor,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: Theme(
                data: _theme.copyWith(
                  primaryColor: _theme.iconTheme.color,
                ),
                child: Consumer<AuthProvider>(
                  builder: (_, state, child) => FormBuilderTextField(
                    name: 'password',
                    textAlignVertical: TextAlignVertical.center,
                    obscureText: state.obsurceText,
                    cursorColor: _theme.iconTheme.color,
                    style: TextStyle(
                      fontFamily: CommonFonts.body,
                      color: _theme.iconTheme.color,
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    decoration: fbLoginTextFieldStyle(
                      labelText: "Password",
                      icon: CommonIcons.password,
                      theme: _theme,
                      suffixIsExist: true,
                      suffixIcon: (state.obsurceText)
                          ? CommonIcons.hide
                          : CommonIcons.show,
                      onPressedSuffixIcon: () =>
                          context.read<AuthProvider>().toggleSuffixIcon(),
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(),
                    ]),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                  onPressed: () => Navigator.pushNamed(context, '/sign-up'),
                  style: TextButton.styleFrom(
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                  ),
                  child: const Text('Don\'t have an account ?'),
                ),
                TextButton(
                  onPressed: () => _resetPasswordModalBottomSheet(context),
                  style: TextButton.styleFrom(
                    minimumSize: Size.zero,
                    padding: EdgeInsets.zero,
                  ),
                  child: const Text('Forgot ?'),
                )
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              width: double.infinity,
              height: 60.0,
              child: Consumer<AuthProvider>(
                builder: (_, state, child) => ElevatedButton(
                    onPressed: state.loading
                        ? null
                        : () {
                            if (_fbKey.currentState!.saveAndValidate()) {
                              final formDoc = LogInModel.fromJson(
                                  _fbKey.currentState!.value);
                              context
                                  .read<AuthProvider>()
                                  .logIn(formDoc: formDoc, context: context);
                            } else {
                              String errorText = getFormErrorText(
                                      formState: _fbKey.currentState!)
                                  .toLowerCase()
                                  .capitalize();

                              Alert().show(
                                  message: errorText,
                                  alertIcon: CommonIcons.alertError,
                                  alertBackGroundColor: CommonColors.errorLight,
                                  context: context);
                            }
                          },
                    child: Text(
                      state.loading ? 'LOGGING IN ...' : 'LOGIN',
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void _resetPasswordModalBottomSheet(
  BuildContext context,
) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Wrap(
          children: <Widget>[
            ListTile(
              leading: const Icon(CommonIcons.telephone),
              title: const Text(
                'Phone Number',
                style: TextStyle(fontFamily: CommonFonts.body),
              ),
              onTap: () {
                Navigator.pop(context);
                context
                    .read<ResetPasswordProvider>()
                    .setType(type: ResetPasswordBy.phone);
                Navigator.pushNamed(context, '/reset-password');
              },
            ),
            ListTile(
              leading: const Icon(CommonIcons.email),
              title: const Text(
                'Email',
                style: TextStyle(fontFamily: CommonFonts.body),
              ),
              onTap: () async {
                Navigator.pop(context);
                context
                    .read<ResetPasswordProvider>()
                    .setType(type: ResetPasswordBy.email);
                Navigator.pushNamed(context, '/reset-password');
              },
            ),
          ],
        );
      });
}
