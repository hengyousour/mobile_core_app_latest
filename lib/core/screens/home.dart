// import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import '../utils/alert.dart';
import '/core/utils/static_theme.dart';
import 'package:provider/provider.dart';
import 'drawer.dart';
import '../providers/connection_provider.dart';
import 'page_structure.dart';
import '../providers/menu_provider.dart';
import '../providers/theme_provider.dart';
import '../utils/constants.dart';
import '../../route_generator.dart';

class Home extends StatefulWidget {
  static List<MenuClass> mainMenu = const [
    MenuClass("drawer.dashboard", CommonIcons.dashborad, 0),
    MenuClass("drawer.profile", CommonIcons.user, 1),
    MenuClass("drawer.setting", CommonIcons.setting, 2),
  ];

  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _drawerController = ZoomDrawerController();
  final int _currentPage = 0;

  @override
  void initState() {
    super.initState();
    context.read<ConnectionProvider>().realTimeConnection();
  }

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
    final isRtl = context.locale.languageCode == "ar";
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        locale: context.locale,
        supportedLocales: const [
          Locale('en'),
        ],
        localizationsDelegates: const [
          FormBuilderLocalizations.delegate,
        ],
        themeMode: themeProvider.themeMode,
        theme: StaticTheme.theme.lightTheme,
        darkTheme: StaticTheme.theme.darkTheme,
        onGenerateRoute: RouteGenerator.generateRoute,
        home: ZoomDrawer(
          controller: _drawerController,
          menuScreen: Menu(
            Home.mainMenu,
            callback: _updatePage,
            current: _currentPage,
          ),
          mainScreen: const MainScreen(),
          openCurve: Curves.fastOutSlowIn,
          showShadow: true,
          slideWidth: MediaQuery.of(context).size.width * (isRtl ? .55 : 0.65),
          isRtl: isRtl,
          mainScreenTapClose: true,
          // mainScreenOverlayColor: Colors.brown.withOpacity(0.5),
          borderRadius: 20,
          angle: 0.0,
          // menuScreenWidth: double.infinity,
          moveMenuScreen: false,
          style: DrawerStyle.defaultStyle,
          // drawerShadowsBackgroundColor: Colors.yellow,
          menuBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
          mainScreenAbsorbPointer: false,
          // boxShadow: [
          //   BoxShadow(
          //     color: Colors.grey.withOpacity(0.5),
          //     spreadRadius: 5,
          //     blurRadius: 7,
          //     offset: Offset(0, 3), // changes position of shadow
          //   ),
          // ],
        ));
  }

  void _updatePage(int index) {
    context.read<MenuProvider>().updateCurrentPage(index);
    _drawerController.toggle?.call();
  }
}

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    final rtl = context.locale.languageCode == "ar";
    return ValueListenableBuilder<DrawerState>(
      valueListenable: ZoomDrawer.of(context)!.stateNotifier,
      builder: (context, state, child) {
        return AbsorbPointer(
          absorbing: state != DrawerState.closed,
          child: WillPopScope(
              onWillPop: Device.get().isAndroid
                  ? () async {
                      if (state == DrawerState.closed) {
                        ZoomDrawer.of(context)?.toggle.call();
                        return false;
                      } else {
                        Alert().showAlertDialog(
                            title: const Center(child: Text('Confirmation')),
                            content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: const [
                                  Text('Do you want to exit an App ?')
                                ]),
                            context: context,
                            cancelBtnLabel: 'No',
                            onCancel: () {
                              Navigator.of(context, rootNavigator: true).pop();
                            },
                            agreeBtnLabel: 'Yes',
                            onAgree: () {
                              Navigator.of(context, rootNavigator: true).pop();
                              SystemNavigator.pop();
                            });
                        return false;
                      }
                    }
                  : null,
              child: child!),
        );
      },
      child: GestureDetector(
        child: const PageStructure(),
        onPanUpdate: (details) {
          if (details.delta.dx < 6 && !rtl || details.delta.dx < -6 && rtl) {
            ZoomDrawer.of(context)?.toggle.call();
          }
        },
      ),
    );
  }
}
