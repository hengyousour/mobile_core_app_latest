import 'package:flutter/material.dart';
import 'package:animate_do/animate_do.dart';
import 'package:provider/provider.dart';
import '../providers/connection_provider.dart';
import '../widgets/real_time_connection.dart';
import '../widgets/responsive.dart';
import '../widgets/login/login_form.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final int _year = DateTime.now().year;

  @override
  void initState() {
    super.initState();
    context.read<ConnectionProvider>().realTimeConnection();
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Stack(children: [
            SizedBox(
              height: double.infinity,
              width: double.infinity,
              child: Center(
                child: SingleChildScrollView(
                  child: FadeIn(
                    duration: const Duration(milliseconds: 1800),
                    child: Column(
                      children: <Widget>[
                        Text('RABBIT', style: theme.textTheme.displayLarge),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Text('welcome back, have a good day !'.toUpperCase(),
                            style: theme.textTheme.titleLarge),
                        const SizedBox(
                          height: 20.0,
                        ),
                        Responsive(
                          desktop: Row(
                            children: [
                              const Spacer(),
                              Expanded(
                                child: LoginForm(theme: theme),
                              ),
                              const Spacer(),
                            ],
                          ),
                          tablet: Row(
                            children: [
                              const Spacer(),
                              Expanded(flex: 6, child: LoginForm(theme: theme)),
                              const Spacer(),
                            ],
                          ),
                          mobile: LoginForm(theme: theme),
                        ),
                        Text(
                          'Copyright \u00a9 $_year Rabbit Technology. All Rights Reserved.',
                          textAlign: TextAlign.center,
                          style: theme.textTheme.bodyLarge,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
      bottomSheet: Selector<ConnectionProvider, bool>(
        selector: (_, state) => state.isConnected,
        builder: (_, isConnected, child) =>
            !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
      ),
    );
  }
}
