import 'package:flutter/material.dart';
import '../widgets/responsive.dart';
import '../widgets/sign-up/sign_up_form.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Responsive(
                    desktop: Row(
                      children: [
                        const Spacer(),
                        Expanded(
                          child: SignUpForm(),
                        ),
                        const Spacer(),
                      ],
                    ),
                    tablet: Row(
                      children: [
                        const Spacer(),
                        Expanded(flex: 6, child: SignUpForm()),
                        const Spacer(),
                      ],
                    ),
                    mobile: SignUpForm(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
