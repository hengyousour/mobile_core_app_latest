import 'dart:math' show pi;
import '../providers/connection_provider.dart';
import '../widgets/real_time_connection.dart';
import 'home.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:provider/provider.dart';
import '../utils/constants.dart';
import '../providers/menu_provider.dart';
import '../screens/dashboard.dart';
import '../screens/profile.dart';
import '../screens/setting.dart';

class PageStructure extends StatelessWidget {
  final String? title;
  final Widget? child;
  final List<Widget>? actions;
  final Color? backgroundColor;
  final double? elevation;

  const PageStructure({
    Key? key,
    this.title,
    this.child,
    this.actions,
    this.backgroundColor,
    this.elevation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final angle = context.locale.languageCode == "en" ? 180 * pi / 180 : 0.0;
    final currentPage =
        context.select<MenuProvider, int>((provider) => provider.currentPage);
    Widget page;

    switch (currentPage) {
      case 0:
        page = const Dashboard();
        break;
      case 1:
        page = const Profile();
        break;
      case 2:
        page = const Setting();
        break;
      default:
        page = const Dashboard();
    }

    final theme = Theme.of(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: elevation,
        centerTitle: true,
        title: Text(
          Home.mainMenu[currentPage].title.tr().toUpperCase(),
          style: theme.textTheme.titleLarge!
              .copyWith(color: theme.appBarTheme.titleTextStyle!.color),
        ),
        leading: Transform.rotate(
          angle: angle,
          child: IconButton(
            icon: Icon(
              CommonIcons.menu,
              color: theme.iconTheme.color,
            ),
            onPressed: () {
              ZoomDrawer.of(context)!.toggle();
            },
          ),
        ),
        actions: actions,
      ),

      // appBar: PlatformAppBar(
      //   automaticallyImplyLeading: false,
      //   material: (_, __) => MaterialAppBarData(
      //     elevation: elevation,
      //     centerTitle: true,
      //     backgroundColor: Colors.transparent,
      //   ),
      //   title: PlatformText(
      //     Home.mainMenu[_currentPage].title,
      //     style: TextStyle(
      //       color: _theme.iconTheme.color,
      //       fontFamily: CommonFonts.header,
      //     ),
      //   ),
      //   leading: Transform.rotate(
      //     angle: angle,
      //     child: IconButton(
      //       icon: Icon(
      //         CommonIcons.menu,
      //         color: _theme.iconTheme.color,
      //       ),
      //       onPressed: () {
      //         ZoomDrawer.of(context).toggle();
      //       },
      //     ),
      //   ),
      // trailingActions: actions,
      // ),
      // bottomNavBar:
      //         PlatformNavBar(
      //             currentIndex: _currentPage,
      //             itemChanged: (index) =>
      //                 Provider.of<MenuProvider>(context, listen: false)
      //                     .updateCurrentPage(index),
      //             items: HomeScreen.mainMenu
      //                 .map(
      //                   (item) => BottomNavigationBarItem(
      //                     title: Text(
      //                       item.title,
      //                       style: style,
      //                     ),
      //                     icon: Icon(
      //                       item.icon,
      //                       color: color,
      //                     ),
      //                   ),
      //                 )
      //                 .toList(),
      //           )
      // ,
      body: Stack(
        children: [
          page,
        ],
      ),
      bottomSheet: Selector<ConnectionProvider, bool>(
        selector: (_, state) => state.isConnected,
        builder: (_, isConnected, child) =>
            !isConnected ? const RealTimeConnection() : const SizedBox.shrink(),
      ),
    );
  }
}
